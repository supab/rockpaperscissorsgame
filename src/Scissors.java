import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class Scissors extends Rock {

	public Scissors(float x, float y) {
		super(x, y);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(Graphics g) {
		if (image == null) {
			try {
				image = new Image("res/scissors.jpg");
			} catch (SlickException e) {
				e.printStackTrace();
			}
		}
		image.draw(getX(), getY());
	}

}
