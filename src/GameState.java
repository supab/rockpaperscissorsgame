import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class GameState extends BasicGameState {
	Image button = null;

	@Override
	public void init(GameContainer arg0, StateBasedGame arg1)
			throws SlickException {
		button = new Image("start.png");

	}

	@Override
	public void render(GameContainer arg0, StateBasedGame arg1, Graphics arg2)
			throws SlickException {
		// TODO Auto-generated method stub
		button.draw(250, 180);
	}

	@Override
	public void update(GameContainer container, StateBasedGame sbg, int arg2)
			throws SlickException {
		Input input = container.getInput();
		if (input.isMousePressed(0)) {
			sbg.enterState(1);
		}
	}

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return 0;
	}

}
