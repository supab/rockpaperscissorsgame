import org.newdawn.slick.Animation;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

public class Enemy extends PlayerStand {

	protected static float x;
	protected static float y;
	private SpriteSheet penguinstandsheet;
	private Animation penguinstandanimation;

	public Enemy(float x, float y) {
		super(x, y);
		penguinstandsheet = null;
	}

	@Override
	public void render(Graphics g) {
		if (penguinstandsheet == null) {
			try {
				penguinstandsheet = new SpriteSheet("res/pgstand.png", 76, 70);
				penguinstandanimation = new Animation(penguinstandsheet, 100);
			} catch (SlickException e) {
				e.printStackTrace();
			}
		}
		penguinstandanimation.draw(x, y);
	}

	protected void setXY(float x, float y) {
		Enemy.x = x;
		Enemy.y = y;
	}
}
