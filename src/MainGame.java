import org.lwjgl.input.Mouse;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.Sound;

public class MainGame extends BasicGameState {
	public static final int GAME_WIDTH = 640;
	public static final int GAME_HEIGHT = 400;
	public static final int X_WEAPON = 50;
	public static final int Y_WEAPON = 335;
	public static final int SPACE_WEAPON = 80;
	public static final int HSIZE_WEAPON = 60;
	public static final int EGDE_WEAPON = SPACE_WEAPON - HSIZE_WEAPON;
	public static final int HPX = 70;
	public static final int HPY = 70;
	public static final int ENEMYHP_X = 370;
	public static final int ENEMYHP_Y = 70;
	public static final int X_HEAL = 70;
	public static final int Y_HEAL = 340;

	Image backgrond = null;

	Image paper = null;
	Image rock = null;
	Image scissors = null;
	Image heal = null;
	Image skillpic = null;

	Image hp5 = null;
	Image hp4 = null;
	Image hp3 = null;
	Image hp2 = null;
	Image hp1 = null;
	Image hp6 = null;
	Image hp7 = null;
	
	Sound atksound;
	Sound gethitsound;

	int userChoice;
	int computerChoice;
	int ROCK = 1;
	int PAPER = 2;
	int SCISSORS = 3;
	int HP = 5;
	int EnemyHP = 7;
	int round = 0;

	boolean isWin = false;
	boolean isLose = false;
	boolean isReady = false;
	boolean isRunning = false;
	boolean isRock = false;
	boolean isPaper = false;
	boolean isScissors = false;
	boolean isEnemyRock = false;
	boolean isEnemyPaper = false;
	boolean isEnemyScissors = false;
	boolean isTie = false;
	boolean isHeal = false;
	boolean isSkill = false;

	String status = "";
	Enemy enemy;
	EnemyRun enemyrun;
	Rock rockplayer, rockenemy;
	Paper paperplayer, paperenemy;
	Scissors scissorsplayer, scissorsenemy;
	PlayerStand playerstand;
	PlayerWalk playerwalk;
	PlayerHit playerhit;

	public MainGame() {
		super();
	}

	@Override
	public void init(GameContainer container, StateBasedGame arg1)
			throws SlickException {
		backgrond = new Image("res/backgroundwolf.jpg");
		scissors = new Image("res/scissors.png");
		rock = new Image("res/rock.png");
		paper = new Image("res/paper.png");

		hp1 = new Image("res/hp1.png");
		hp2 = new Image("res/hp2.png");
		hp3 = new Image("res/hp3.png");
		hp4 = new Image("res/hp4.png");
		hp5 = new Image("res/hp5.png");
		hp6 = new Image("res/hp6.png");
		hp7 = new Image("res/hp7.png");
		heal = new Image("res/heal.png");
		skillpic = new Image("res/skilldmg.png");

		playerwalk = new PlayerWalk(50, 245);
		playerstand = new PlayerStand(50, 245);
		playerhit = new PlayerHit(451, 230);
		enemy = new Enemy(520, 260);
		enemyrun = new EnemyRun(520, 265);
		
//		gethitsound = new Sound("res/gethit.oog");
		
		rockplayer = new Rock(110, 100);
		paperplayer = new Paper(110, 100);
		scissorsplayer = new Scissors(110, 100);
		rockenemy = new Rock(360, 100);
		paperenemy = new Paper(360, 100);
		scissorsenemy = new Scissors(360, 100);

		container.setMinimumLogicUpdateInterval(1000 / 60);
	}

	@Override
	public void render(GameContainer arg0, StateBasedGame sbg, Graphics g)
			throws SlickException {
		drawImageString(g);
		hpRender(sbg, g);
		gameRender(g);
	}

	public void drawImageString(Graphics g) {
		g.drawImage(backgrond, 0, 0);
		g.drawImage(rock, X_WEAPON, Y_WEAPON);
		g.drawImage(paper, X_WEAPON + SPACE_WEAPON, Y_WEAPON);
		g.drawImage(scissors, X_WEAPON + 2 * SPACE_WEAPON, Y_WEAPON);
		g.drawImage(heal, X_HEAL, 20);

		g.drawImage(skillpic, X_HEAL + 50, 20);
		g.drawString("Round " + round, 285, 50);
		g.drawString("" + status, 285, 100);
		g.drawString("" + HP, 70, 230);
		g.drawString("" + EnemyHP, 550, 240);
	}

	public void hpRender(StateBasedGame sbg, Graphics g) {
		if (HP > 5)
			HP = 5;
		if (HP == 5)
			g.drawImage(hp5, HPX, HPX);
		if (HP == 4)
			g.drawImage(hp4, HPX, HPY);
		if (HP == 3)
			g.drawImage(hp3, HPX, HPY);
		if (HP == 2)
			g.drawImage(hp2, HPX, HPY);
		if (HP == 1)
			g.drawImage(hp1, HPX, HPY);
		if (HP == 0)
			sbg.enterState(3);

		if (EnemyHP == 7)
			g.drawImage(hp7, ENEMYHP_X, ENEMYHP_Y);
		if (EnemyHP == 6)
			g.drawImage(hp6, ENEMYHP_X, ENEMYHP_Y);
		if (EnemyHP == 5)
			g.drawImage(hp5, ENEMYHP_X, ENEMYHP_Y);
		if (EnemyHP == 4)
			g.drawImage(hp4, ENEMYHP_X, ENEMYHP_Y);
		if (EnemyHP == 3)
			g.drawImage(hp3, ENEMYHP_X, ENEMYHP_Y);
		if (EnemyHP == 2)
			g.drawImage(hp2, ENEMYHP_X, ENEMYHP_Y);
		if (EnemyHP == 1)
			g.drawImage(hp1, ENEMYHP_X, ENEMYHP_Y);
		if (EnemyHP == 0)
			sbg.enterState(4);
	}

	public void gameRender(Graphics g) {
		if (isLose == false)
			enemy.render(g);
		if (isLose == true)
			enemyrun.render(g);
		if (isWin == false)
			playerstand.render(g);
		if (isWin == true && isReady == false)
			playerwalk.render(g);
		if (isWin == true && isReady == true)
			playerhit.render(g);

		if (isRock == true)
			rockplayer.render(g);
		if (isPaper == true)
			paperplayer.render(g);
		if (isScissors == true)
			scissorsplayer.render(g);

		if (isEnemyRock == true)
			rockenemy.render(g);
		if (isEnemyPaper == true)
			paperenemy.render(g);
		if (isEnemyScissors == true)
			scissorsenemy.render(g);

		if (isTie == true) {
			isWin = false;
			isRunning = false;
			isTie = false;
			isRock = false;
			isPaper = false;
			isScissors = false;
			isEnemyRock = false;
			isEnemyPaper = false;
			isEnemyScissors = false;
		}
	}

	@Override
	public void update(GameContainer container, StateBasedGame sbg, int delta)
			throws SlickException {
		Input input = container.getInput();
		updateComputerInput();
		updateMouseInput(input);
		checkWin();
		gameUpdate(delta);
		checkCollision();
	}

	public void gameUpdate(int delta) {
		if (isWin == true && isReady == false)
			playerwalk.update(delta);
		if (isWin == true && isReady == true)
			playerhit.update(delta);
		if (isLose == true)
			enemyrun.update(delta);
	}

	public void updateComputerInput() {
		final double d = Math.random();
		if (d < .33) {
			computerChoice = ROCK;
		} else if (d < .66) {
			computerChoice = PAPER;
		} else {
			computerChoice = SCISSORS;
		}
	}

	public void updateMouseInput(Input input) {
		int xpos = Mouse.getX();
		int ypos = Mouse.getY();
		if (isRunning == false) {
			if ((xpos > X_WEAPON && xpos < X_WEAPON + HSIZE_WEAPON)
					&& (ypos > 15 && ypos < 65)) {
				if (input.isMousePressed(0)) {
					userChoice = ROCK;
					round++;
					isRunning = true;
					isRock = true;
				}
			}
			if ((xpos > X_WEAPON + SPACE_WEAPON && xpos < X_WEAPON
					+ SPACE_WEAPON + HSIZE_WEAPON)
					&& (ypos > 15 && ypos < 65)) {
				if (input.isMousePressed(0)) {
					userChoice = PAPER;
					round++;
					isRunning = true;
					isPaper = true;
				}
			}
			if ((xpos > X_WEAPON + 2 * SPACE_WEAPON && xpos < X_WEAPON + 2
					* SPACE_WEAPON + HSIZE_WEAPON)
					&& (ypos > 15 && ypos < 65)) {
				if (input.isMousePressed(0)) {
					userChoice = SCISSORS;
					round++;
					isRunning = true;
					isScissors = true;
				}
			}
			if ((xpos > X_HEAL && xpos < X_HEAL + 40)
					&& (ypos > Y_HEAL && ypos < Y_HEAL + 40)) {
				if (input.isMousePressed(0) && isHeal == false) {
					HP++;
					isHeal = true;
					System.out.println("heal");
				}
			}
			if ((xpos > X_HEAL + 50 && xpos < X_HEAL + 90)
					&& (ypos > Y_HEAL && ypos < Y_HEAL + 40)) {
				if (input.isMousePressed(0) && isSkill == false) {
					isSkill = true;
					EnemyHP--;
					System.out.println("skill");
				}
			}
		}
	}

	public void checkCollision() {
		if (CollisionDetector.isCollide(PlayerHit.x) == true) {
			EnemyHP--;
			isWin = false;
			isRunning = false;
			isTie = false;
			isRock = false;
			isPaper = false;
			isScissors = false;
			isEnemyRock = false;
			isEnemyPaper = false;
			isEnemyScissors = false;
			isReady = false;
			PlayerHit.x = 451;
			PlayerHit.y = 245;
			PlayerWalk.x = 50;
		}

		if (CollisionDetector.isHit(PlayerStand.x) == true) {
//			gethitsound.play(1f, 1f);
			HP--;
			EnemyRun.x = 520;
			EnemyRun.y = 245;
			isLose = false;
			isRunning = false;
			isTie = false;
			isRock = false;
			isPaper = false;
			isScissors = false;
			isEnemyRock = false;
			isEnemyPaper = false;
			isEnemyScissors = false;
		}

		if (CollisionDetector.isReady(PlayerWalk.x) == true) {
			isReady = true;
		}
	}

	public void checkWin() {
		if (userChoice == ROCK) {
			if (computerChoice == PAPER) {
				userChoice = 4;
				isLose = true;
				status = "Lose!!!";
				isEnemyPaper = true;
			} else if (computerChoice == SCISSORS) {
				userChoice = 4;
				isWin = true;
				status = "Win!!!";
				isEnemyScissors = true;
			} else if (userChoice == computerChoice) {
				userChoice = 4;
				isRunning = false;
				status = "Draw!!!";
				isEnemyRock = true;
				isTie = true;
			}
		} else if (userChoice == PAPER) {
			if (computerChoice == SCISSORS) {
				userChoice = 4;
				isLose = true;
				status = "Lose!!!";
				isEnemyScissors = true;
			} else if (computerChoice == ROCK) {
				userChoice = 4;
				isWin = true;
				status = "Win!!!";
				isEnemyRock = true;
			} else if (userChoice == computerChoice) {
				userChoice = 4;
				isRunning = false;
				status = "Draw!!!";
				isEnemyPaper = true;
				isTie = true;
			}
		} else if (userChoice == SCISSORS) {
			if (computerChoice == ROCK) {
				userChoice = 4;
				isLose = true;
				status = "Lose!!!";
				isEnemyRock = true;
			} else if (computerChoice == PAPER) {
				userChoice = 4;
				isWin = true;
				status = "Win!!!";
				isEnemyPaper = true;
			} else if (userChoice == computerChoice) {
				userChoice = 4;
				isRunning = false;
				status = "Draw!!!";
				isEnemyScissors = true;
				isTie = true;
			}
		}
	}

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return 2;
	}
}
