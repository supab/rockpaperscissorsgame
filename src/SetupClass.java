import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class SetupClass extends StateBasedGame {

	public static final int GAME_WIDTH = 640;
	public static final int GAME_HEIGHT = 400;

	public SetupClass(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void initStatesList(GameContainer arg0) throws SlickException {
		this.addState(new GameState());
		this.addState(new WeaponChoice());
		this.addState(new MainGame());
		this.addState(new GameOver());
		this.addState(new GameWin());
	}

	public static void main(String[] args) {
		try {
			SetupClass game = new SetupClass("Rock-Paper-Scissors");
			AppGameContainer appgc = new AppGameContainer(game);
			appgc.setDisplayMode(GAME_WIDTH, GAME_HEIGHT, false);
			appgc.start();

		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
}
