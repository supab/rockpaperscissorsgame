import org.newdawn.slick.Animation;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

public class EnemyRun extends PlayerStand {

	protected static float x;
	protected static float y;
	private SpriteSheet penguinrunsheet;
	private Animation penguinrunanimation;

	public EnemyRun(float x, float y) {
		super(x, y);
		penguinrunsheet = null;
	}

	@Override
	public void render(Graphics g) {
		if (penguinrunsheet == null) {
			try {
				penguinrunsheet = new SpriteSheet("res/pgrun.png", 80, 70);
				penguinrunanimation = new Animation(penguinrunsheet, 100);
			} catch (SlickException e) {
				e.printStackTrace();
			}
		}
		penguinrunanimation.draw(x, y);
	}

	@Override
	public void update(int delta) {
		updateMovement();
	}

	protected void updateMovement() {
		x -= 3;
	}

	protected void setXY(float x, float y) {
		EnemyRun.x = x;
		EnemyRun.y = y;
	}
}
