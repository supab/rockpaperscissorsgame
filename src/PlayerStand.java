import org.newdawn.slick.Animation;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

public class PlayerStand {

	protected static float x;
	protected static float y;
	private SpriteSheet playersheet;
	private Animation playerstandanimation;

	public PlayerStand(float x, float y) {
		playersheet = null;
		this.setXY(x, y);
	}

	public void render(Graphics g) {
		if (playersheet == null) {
			try {
				playersheet = new SpriteSheet("res/playerstand.png", 84, 80);
				playerstandanimation = new Animation(playersheet, 100);
			} catch (SlickException e) {
				e.printStackTrace();
			}
		}
		playerstandanimation.draw(x, y);
	}

	public void update(int delta) {

		updateMovement();
		updatePosition();
	}

	private void updatePosition() {
		this.setXY(x, y);
	}

	protected void updateMovement() {
		x -= 0;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	protected void setXY(float x, float y) {
		PlayerStand.x = x;
		PlayerStand.y = y;
	}
}
