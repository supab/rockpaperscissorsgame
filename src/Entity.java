
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public interface Entity {
  void render(Graphics g) throws SlickException;
  void update(int delta);
}