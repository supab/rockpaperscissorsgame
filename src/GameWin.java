import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;


public class GameWin extends BasicGameState{

	Image gamewin = null;
	PlayerWalk playerwalk;
	
	@Override
	public void init(GameContainer arg0, StateBasedGame arg1)
			throws SlickException {
		gamewin = new Image("res/win.png");
		playerwalk = new PlayerWalk(50, 245);
	}

	@Override
	public void render(GameContainer arg0, StateBasedGame arg1, Graphics g)
			throws SlickException {
		g.drawImage(gamewin, 0, 0);
		playerwalk.render(g);
		
	}

	@Override
	public int getID() {
		return 4;
	}

	@Override
	public void update(GameContainer arg0, StateBasedGame arg1, int arg2)
			throws SlickException {
		playerwalk.update(arg2);
	}
}

