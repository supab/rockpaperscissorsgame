import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;


public class GameOver extends BasicGameState{

	Image gameover = null;

	@Override
	public void init(GameContainer arg0, StateBasedGame arg1)
			throws SlickException {
		gameover = new Image("res/gameover.png");
	}

	@Override
	public void render(GameContainer arg0, StateBasedGame arg1, Graphics g)
			throws SlickException {
		g.drawImage(gameover, 0, 0);
	}

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return 3;
	}

	@Override
	public void update(GameContainer arg0, StateBasedGame arg1, int arg2)
			throws SlickException {
		// TODO Auto-generated method stub
		
	}
}
