import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;



public class RPSGame extends BasicGame{

	public static final int GAME_WIDTH = 640;
	public static final int GAME_HEIGHT = 400;
	Image backgrond = null;
	
	int userChoice;
	int computerChoice;
	int ROCK = 1;
	int PAPER = 2;
	int SCISSORS = 3;
	int wins = 0;
    int losses = 0;
    int ties = 0;
    int round = 0;
    String isWin = "";
    
	public RPSGame(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}


	@Override
	public void init(GameContainer container) throws SlickException {
		backgrond = new Image("res/bg.jpg");
		updateComputerInput();
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void render(GameContainer container, Graphics g) throws SlickException {
		g.drawImage(backgrond, 0, 0);
		g.drawString(""+ isWin, 250, 200);
		g.drawString("Round "+ round, 250, 220);
		g.drawString("Win "+wins +"Lose "+losses +"Ties"+ties, 250, 250);
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(GameContainer container, int arg1) throws SlickException {
		Input input = container.getInput();
		updateInput(container, input);
		checkwin();
		if(wins >= 5)
			isWin = "You Win!!!";
		if(losses >= 5)
			isWin = "You Lose!!!";
		// TODO Auto-generated method stub
		
	}


	public void updateInput(GameContainer container, Input input)
			throws SlickException {
		if (input.isKeyPressed(Input.KEY_Q)){
			userChoice = ROCK;
			round++;
			container.reinit();
		}
		
		if (input.isKeyPressed(Input.KEY_W)){
			userChoice = SCISSORS;
			round++;
			container.reinit();
		}
		
		if (input.isKeyPressed(Input.KEY_E)){
			userChoice = PAPER;
			round++;
			container.reinit();
		}
	}
	

	
	public void updateComputerInput() {
		final double d = Math.random();
			if(d < .33){
		    	computerChoice = ROCK;
		    }
		    else if(d < .66){
		    	computerChoice = PAPER;
		    }
		    else{
		    	computerChoice = SCISSORS;
		    }
	}

	public void checkwin() {
		if(userChoice == ROCK){
	        if(computerChoice == PAPER){
	            losses++;
	            userChoice = 4;
	        }
	        else if(computerChoice == SCISSORS){
	            //rock smashes scissors
	            wins++;
	            userChoice = 4;
	        }
	        else if(userChoice == computerChoice){
		    	 ties++;
		    	 userChoice = 4;
		    }
	    }
	    else if(userChoice == PAPER){

	        if(computerChoice == SCISSORS){
	            //paper is cut by scissors
	            losses++;
	            userChoice = 4;
	        }
	        else if(computerChoice == ROCK){
	            //paper covers rock
	            wins++;
	            userChoice = 4;
	        }
	        else if(userChoice == computerChoice){
		    	 ties++;
		    	 userChoice = 4;
		    }
	    }
	    else if(userChoice == SCISSORS){

	        if(computerChoice == ROCK){
	            //scissors are smashed by rock
	            losses++;
	            userChoice = 4;
	        }
	        else if(computerChoice == PAPER){
	            //scissors cut paper
	            wins++;
	            userChoice = 4;
	        }
	        else if(userChoice == computerChoice){
		    	 ties++;
		    	 userChoice = 4;
		    }
	    }
	}


	public static void main(String[] args) {
	    try {
	      RPSGame game = new RPSGame("Rock-Paper-Scissors");
	      AppGameContainer appgc = new AppGameContainer(game);
	      appgc.setDisplayMode(GAME_WIDTH, GAME_HEIGHT, false);
	      appgc.start();
	      
	    } catch (SlickException e) {
	      e.printStackTrace();
	    }
	  }
}
