import org.newdawn.slick.Animation;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

public class PlayerWalk extends PlayerStand {
	protected static float x;
	protected static float y;
	private SpriteSheet playerrunsheet;
	private Animation playerrunanimation;

	public PlayerWalk(float x, float y) {
		super(x, y);
		playerrunsheet = null;
	}

	public void render(Graphics g) {
		if (playerrunsheet == null) {
			try {
				playerrunsheet = new SpriteSheet("res/playerrun.png", 83, 80);
				playerrunanimation = new Animation(playerrunsheet, 100);
			} catch (SlickException e) {
				e.printStackTrace();
			}
		}
		playerrunanimation.draw(x, y);
	}

	public void update(int delta) {
		updateMovement();
	}

	protected void updateMovement() {
		x += 3;
	}

	protected void setXY(float x, float y) {
		PlayerWalk.x = x;
		PlayerWalk.y = y;
	}

}
