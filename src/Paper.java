import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class Paper extends Rock {

	public Paper(float x, float y) {
		super(x, y);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(Graphics g) {
		if (image == null) {
			try {
				image = new Image("res/paper.jpg");
			} catch (SlickException e) {
				e.printStackTrace();
			}
		}
		image.draw(getX(), getY());
	}

}
