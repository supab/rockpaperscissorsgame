import org.newdawn.slick.Animation;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

public class PlayerHit extends PlayerStand {
	protected static float x;
	protected static float y;
	private SpriteSheet playerhitsheet;
	private Animation playerhitanimation;

	public PlayerHit(float x, float y) {
		super(x, y);
		playerhitsheet = null;
	}

	@Override
	public void render(Graphics g) {
		if (playerhitsheet == null) {
			try {
				playerhitsheet = new SpriteSheet("res/playerhit.png", 117, 100);
				playerhitanimation = new Animation(playerhitsheet, 100);
			} catch (SlickException e) {
				e.printStackTrace();
			}
		}
		playerhitanimation.draw(x, y);
	}

	@Override
	public void update(int delta) {
		updateMovement();
	}

	protected void updateMovement() {
		x += 3;
	}

	protected void setXY(float x, float y) {
		PlayerHit.x = x;
		PlayerHit.y = y;
	}
}
