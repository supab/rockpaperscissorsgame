import org.lwjgl.input.Mouse;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class WeaponChoice extends BasicGameState {

	Image hammer = null;
	Image scissors = null;
	Image paper = null;
	Image choose = null;
	
	public static final int Y_WEAPON = 180;
	public static final int SIZE_WEAPON = 150;
	public static final int SPACE_WEAPON = 50;

	@Override
	public void init(GameContainer arg0, StateBasedGame arg1)
			throws SlickException {
		hammer = new Image("res/hammer.jpg");
		scissors = new Image("res/scissors.jpg");
		paper = new Image("res/paper.jpg");
		choose = new Image("res/choose.png");

	}

	@Override
	public void render(GameContainer arg0, StateBasedGame arg1, Graphics g)
			throws SlickException {
		g.drawImage(hammer, 50, Y_WEAPON);
		g.drawImage(scissors, 250, Y_WEAPON);
		g.drawImage(paper, 450, Y_WEAPON);
		g.drawImage(choose, 70, 30);
	}

	@Override
	public void update(GameContainer container, StateBasedGame sbg, int arg2)
			throws SlickException {
		Input input = container.getInput();
		updateMouseinputMenu(sbg, input);
	}

	public void updateMouseinputMenu(StateBasedGame sbg, Input input) {
		int xpos = Mouse.getX();
		int ypos = Mouse.getY();
		if ((xpos > SPACE_WEAPON && xpos < SPACE_WEAPON + SIZE_WEAPON)
				&& (ypos > 70 && ypos < 70 + SIZE_WEAPON)) {
			if (input.isMousePressed(0)) {
				sbg.enterState(2);
			}
		}
		if ((xpos > 2 * SPACE_WEAPON + SIZE_WEAPON && xpos < 2 * SPACE_WEAPON
				+ 2 * SIZE_WEAPON)
				&& (ypos > 70 && ypos < 70 + SIZE_WEAPON)) {
			if (input.isMousePressed(0)) {
				sbg.enterState(2);
			}
		}
		if ((xpos > 3 * SPACE_WEAPON + 2 * SIZE_WEAPON && xpos < 3
				* SPACE_WEAPON + 3 * SIZE_WEAPON)
				&& (ypos > 70 && ypos < 70 + SIZE_WEAPON)) {
			if (input.isMousePressed(0)) {
				sbg.enterState(2);
			}
		}
	}

	public static int userChoice(int userChoice) {
		return userChoice;
	}

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return 1;
	}

}
