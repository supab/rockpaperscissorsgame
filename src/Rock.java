import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class Rock  {
	Image rock = null;
	protected float x;
	protected float y;
	protected Image image;

	public Rock(float x, float y) {
		image = null;
		this.setXY(x, y);
	}

	public void render(Graphics g) {
		if (image == null) {
			try {
				image = new Image("res/hammer.jpg");
			} catch (SlickException e) {
				e.printStackTrace();
			}
		}
		image.draw(getX(), getY());
	}

	public void update(int delta) {
		updateMovement();
		updatePosition();
	}

	private void updatePosition() {
		this.setXY(x, y);
	}

	protected void updateMovement() {
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	protected void setXY(float x, float y) {
		this.x = x;
		this.y = y;
	}
}